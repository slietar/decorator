/**
 *
 * decorator
 * TypeScript definition
 *
 **/

declare module '@slietar/decorator' {
  export enum TargetKind { Class, Method, Parameter, Property }

  export interface DecoratorHandlers<A> { // args, target
    class?: (...args: A[]) => void;
    method?: (...args: A[]) => void;
    parameter?: (...args: A[]) => void;
    property?: (...args: A[]) => void;
  }

  export type AnyDecorator = ClassDecorator & MethodDecorator & ParameterDecorator & PropertyDecorator;
  export type DecoratorFactory<A> = (...args: A[]) => AnyDecorator;

  export function Decorator<A>(handlers: DecoratorHandlers<A>): DecoratorFactory<A>;
  export function CombinedDecorator<A, T>(handler: (...args: A[]) => Function | TypedPropertyDescriptor<T> | void);

  export var deprecate: DecoratorFactory<[string, number]>;
}
