# Decorator

## Example

```js
import { Decorator, TargetKind } from 'decorator';

let transform = decorator({
  [TargetKind.Parameter]: function (type) {
    this.cover(function (arg) {
      if (type === 'uppercase') return arg.toUpperCase();
      else if (type === 'lowercase') return arg.toLowerCase();
      // nothing
    });
  }
});
```

```js
class A {
  setLastName(@transform('uppercase') name) {
    this.lastName = name;
  }
}

let a = new A();
a.setName('Doe');

console.log(a.lastName); // outputs `DOE`
```

Note: you must include the `decorate` library first

## Patterns

| Agent method                 | Pattern                 |
|------------------------------|-------------------------|
| `ClassAgent#cover()`         | `(A) ⟼ A`             |
| `ClassAgent#method()`        | `(N) ⟼ MethodAgent`   |
| `ClassAgent#override()`      | `(A) ⟼ R`             |
| `ClassAgent#wrap()`          | `(R) ⟼ R`             |
| `MethodAgent#toProperty()`   | `( ) ⟼ void`          |
| `ParameterAgent#getMethod()` | `( ) ⟼ MethodAgent`   |

## Order

On method:

```
class A {
  @cover1()
  @cover2()
  @wrap()
  @cover3()
  method() {

  }
}
```

Final order:

```
⟼ cover1(input) ⟼ A
⟼ cover2() ⟼ A
⟼ cover3() ⟼ A
⟼ method() ⟼ R
⟼ wrap()   ⟼ output
```
