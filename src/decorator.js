/**
 *
 * decorator
 *
 **/


export const TargetKind = {
  Class: 0,
  Method: 1,
  Parameter: 2,
  Property: 3
};


export class Agent {
  constructor(kind) {
    this.kind = kind;
  }
}

export class ClassAgent extends Agent {
  constructor(target, _kind) {
    super(_kind || TargetKind.Class);

    this.originalTarget = target;
    this.target = target;

    this.signature = this.originalTarget.name;
  }

  cover(handler) { // before
    return this.override((...args) => {
      return new this.originalTarget(...(handler(...args) || args));
    });
  }

  enclose(coverHandler, wrapHandler) { // before and after
    this.originalTarget = this.cover(coverHandler);
    this.wrap(wrapHandler);
    return this.target;
  }

  method(methodName) {
    // ...
  }

  override(handler) {
    this.target = function _classAgentOverride(...args) {
      return handler(...args);
    };
    return this.target;
  }

  wrap(handler) { // after
    this.target = (...args) => {
      let originalResult = new this.originalTarget(...args);
      return handler.call(originalResult) || originalResult;
    };
    return this.target;
  }
}

export class DescriptorAgent extends Agent {
  constructor(kind, descriptor) {
    super(kind);

    this.descriptor = descriptor;
  }

  set configurable(value) {
    this.descriptor.configurable = value;
  }

  set enumerable(value) {
    this.descriptor.enumerable = value;
  }

  set writable(value) {
    this.descriptor.writable = value;
  }
}

export class MethodAgent extends DescriptorAgent {
  constructor(target, key, descriptor, _kind) {
    super(_kind || TargetKind.Method, descriptor);

    this.controlled = false;
    this.key = key;
    this.originalValue = this.descriptor.value;
    this.target = target;

    this.signature = `${this.target.constructor.name}#${this.key}()`;
  }

  control() {
    this.controlled = true;
  }

  cover(handler) {
    let originalValue = this.originalValue;
    return this.override(function (...args) {
      return originalValue.call(this, ...(handler.call(this, ...args) || args));
    });
  }

  desynchronize(handler) {
    let originalValue = this.originalValue;
    return this.override(function (...args) {
      return new Promise((resolve, reject) => {
        let done = newArgs => resolve(newArgs || args);
        handler.call(this, done, ...args);
      }).then((args) => {
        return originalValue.call(this, ...args);
      });
    });
  }

  override(handler) {
    this.descriptor.value = function (...args) {
      return handler.call(this, ...args);
    };
    return this.descriptor;
  }

  wrap(handler) {
    let originalValue = this.originalValue;
    return this.override(function (...args) {
      let originalResult = originalValue.call(this, ...args);
      return handler.call(this, originalResult) || originalResult;
    });
  }
}

export class ParameterAgent extends MethodAgent {
  constructor(target, key, descriptor, paramIndex) {
    super(target, key, descriptor, TargetKind.Parameter);

    this.parameterIndex = paramIndex;
    this.signature = `${this.target.constructor.name}#${this.key}(${this.parameterIndex})`;
  }

  cover(handler) {
    let index = this.parameterIndex;
    return super.cover(function (...args) {
      let originalArg = args[index];
      args[index] = handler.call(this, originalArg) || originalArg;
      return args;
    });
  }

  wrap() {
    throw new Error('Not allowed');
  }
}

export class ConstructorParameterAgent extends ClassAgent {
  constructor(target, paramIndex) {
    super(target, TargetKind.Parameter);

    this.parameterIndex = paramIndex;
    this.signature = `${this.target.name}(${this.parameterIndex})`;
  }

  cover(handler) {
    let index = this.parameterIndex;
    return super.cover(function (...args) {
      let originalArg = args[index];
      args[index] = handler.call(this, originalArg) || originalArg;
      return args;
    });
  }

  wrap() {
    throw new Error('Not allowed');
  }
}

export class PropertyAgent extends DescriptorAgent {
  constructor(target, key, descriptor) {
    super(TargetKind.Property, descriptor);

    this.key = key;
    this.target = target;

    this.signature = `${this.target.constructor.name}#${this.key}`;

    this.originalGet = this.descriptor.get;
    this.originalSet = this.descriptor.set;
  }

  cover(handler) {
    let originalSet = this.originalSet;
    this.descriptor.set = function (...args) {
      return originalSet.call(this, ...(handler.call(this, ...args) || args));
    };
    return this.descriptor;
  }

  wrap(handler) {
    let originalGet = this.originalGet;
    this.descriptor.get = function () {
      let originalResult = originalGet.call(this);
      return handler.call(this, originalResult) || originalResult;
    };
    return this.descriptor;
  }
}


export function Decorator(handlers) {
  return function _decoratorFactory(...decoratorArgs) {
    return function _decorator(target, key, descriptor, paramIndex) {
      let agent = (function _agentFactory() {
        if (typeof target === 'function' && paramIndex !== void 0) {
          return new ConstructorParameterAgent(target, paramIndex);
        } else if (typeof target === 'function') {
          return new ClassAgent(target);
        } else if (paramIndex !== void 0) {
          return new ParameterAgent(target, key, descriptor, paramIndex);
        } else if (typeof descriptor.value === 'function') {
          return new MethodAgent(target, key, descriptor);
        } else {
          return new PropertyAgent(target, key, descriptor);
        }
      })();

      let handler = handlers[agent.kind];

      if (!handler) {
        throw new Error('Decorator not allowed on this target kind');
      }

      return handler.call(agent, ...decoratorArgs) || agent.descriptor || agent.target;
    };
  };
}

export function CombinedDecorator(handler) {
  return Decorator({
    [TargetKind.Class]: handler,
    [TargetKind.Method]: handler,
    [TargetKind.Parameter]: handler,
    [TargetKind.Property]: handler
  });
}
